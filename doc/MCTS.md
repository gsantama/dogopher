<div align='center'>

<img src=https://gitlab.utc.fr/uploads/-/system/project/avatar/14278/dogopher.png alt="logo" width=90 height=90 />

<h1>Dogopher</h1>
<p>Analyses de l'algorithme de MCTS</p>
<a href="../README.md">README</a>
<br/>
</div>

<div align='center'>
    <img src="videos/mcts_alpha_gopher.gif" width="280px">
</div>


Nous avons implémenté l'algorithme de Monte-Carlo Tree-Search pour récupérer les meilleurs coups possible pour une situation donnée et un joueur donné. Le problème majeur que nous avons rencontré pour cet algorithme est l'optimisation de la phase d'entraînement, et notemment des temps passés lors du calcul de la récompense pour les noeuds explorés.

En effet, lors de chaque entraînement, il nous faut simuler une partie pour pouvoir en déduire l'issu et donc inférer une récompense.

Dans les utilisations plus traditionnelles de MCTS, il est utilisé en combinaison avec un réseau de neurones qui infère la récompense à partir d'une situation, sans avoir besoin de simuler une partie entière, ce qui réduit grandement le temps de calcul.

L'avantage de ces optimisations, c'est qu'elles ne concernent pas toutes uniquement la classe MCTS, et donc qu'elle s'appliquent de manière inhérente à l'algorithme `AlphaBeta` que nous avons aussi implémenté.

# Diverses petites optimisations

Nous répertorions ici quelques petites optimisations que nous avons réalisés pour améliorer les performances de notre algorithme MCTS et espérer réduire les temps de calcul des coups.

## Hashage efficace de la grille

Le simple fait de ne pas utiliser de fonctions cryptographiques pour le hashage de notre grille (ainsi que de bien gérer le layout du tableau NumPy) nous a permis de gagner pas mal de temps sur le hashage.

Voir [HASHING.md](HASHING.md)

## Utilisation de vecteurs dans `get_legal_moves`

La fonction que nous avons à optimiser:

```python
def get_legal_moves_old(grid, ply):
    direction = get_direction(ply)

    actions = [
        (cell, neighbour)
        for cell in grid.filter(lambda _, player: player == ply)
        for neighbour in grid.get_neighbours(cell, direction)
    ]

    return [action for action in actions if Dodo.is_legal(grid, action, ply)]
```

Dans la compréension de liste, nous utilisons la fonction `grid.filter` avec une lambda expression qui récupère seulement les cellules qui appartiennent au joueur `ply`.

Bien que ce code soit lisible et assez efficace (globalement, il est en `O(n)`), voici comment est implémentée la fonction `filter`:

```python
def iter(self):
    size = self.size[0]
    limit = size - 1

    for x in range(-limit, size):
        for y in range(-limit, size):
            if self[x, y] == -1:
                continue
            yield (x, y), self[x, y]

def filter(self, f):
    result = []
    for cell, player in self.iter():
        if f(cell, player):
            result.append(cell)
    return result
```

Nous pouvons utiliser avantageusement les fonctions performantes de NumPy pour éviter de n'utiliser trop de boucles natives de Python. On définit alors la fonction `get_cell` qui effectue la même chose que `filter(lambda _, player: player == ply)`:

```python
def get_cells(self, ply):
    cells = np.column_stack(np.where(self.grid == ply))
    
    # Code pour traduire en coordonnées
    # hexagonales
    norm = self.size[0] - 1
    x = cells[:, 0] - norm
    y = cells[:, 1] - norm
    flat = (y, -x)
    
    return np.column_stack(flat)
```

Notre fonction `get_legal_moves` devient alors:

```python
def get_legal_moves(grid, ply):
    direction = get_direction(ply)

    return [
        action
        for action in (
            (cell, neighbour)
            for cell in grid.get_cells(ply)
            for neighbour in grid.get_neighbours(cell, direction)
            if grid[neighbour] == 0
        )
    ]
```

Voici un tableau comparatif des temps d'exécution de la fonction `get_legal_moves` avec et sans l'utilisation de vecteurs.

<div align="center">

| Sans vecteurs (s)       | Avec vecteurs (s)       |
| ----------------------- | ----------------------- |
| 0.00047791774934979273s | 0.00011758569559498608s |

</div>

Ainsi l'utilisation des fonctions vectorisées de NumPy nous permets d'augmenter la vitesse de calcul de `get_legal_moves` d'environ 4.06 fois.

## Mise en cache des voisins

Une autre optimisation que nous pouvons faire (et que nous avons implémenté) est le précalcul des voisins de toutes les cellules. Ainsi, lorsque nous en avons besoin pour, par exemple calculer les coups possible d'un joueur, l'accès aux voisins est en `O(1)`.


## Entraînement de l'arbre en parallèle

Une autre optimisation que nous avons réaliser est de lancer les entraînements de l'arbre en parallèle en utilisant la librairie `mpire` de Python. Nous avons observé d'énormes gains de temps dans les calculs de récompenses pour les noeuds explorés.

La stratégie utilisée pour faire tourner l'entraînement en parrallèle est d'utiliser la même *root* pour tous les processus et de fusioner les résultats à la fin de l'entraînement.

Si nous utilisons le même *root* que nous copions $x$ fois à travers chacun des processus, c'est parce qu'il nous a paru outrageusement compliqué de faire un système qui utilise de la mémoire partagée (voir https://stackoverflow.com/a/59257364 et https://stackoverflow.com/a/14135569 par exemple). De plus, l'interpréteur Python lui-même n'est pas $100\%$ thread-safe (voir: https://docs.python.org/3/c-api/init.html#thread-state-and-the-global-interpreter-lock), il paraissait donc clair que la solution la plus simple était de faire du `copy-on-write`.

Néanmoins, faire ce `copy-on-write` a crée un problème d'utilisation beaucoup trop excessive de la mémoire vive, entraînant un nombre incalculable de *Killed* de la part de notre gentil (mais sévère) OS. Nous avons implémentés quelques petites stratégies pour limiter l'usage de la RAM que vous pouvez retrouver dans le fichier [strategy.py](../game/strategy.py).

# L'algorithme dans les grandes lignes

Nous utilisons l'algorithme de base expliqué dans [4] pour MCTS. Les améliorations que nous avons effectuées pour augmenter la performance de l'algorithme sont contenues dans la manière de gérer les différentes phases de sélection, d'expansion de simulation et de rétropropagation. Les travaux de [5] ont été utilisés et ont montré de grandes améliorations ! L'idée est d'utiliser un minimax pour l'évaluation des noeuds de notre MCTS, de telle manière que par exemple, dans la phase de simulation, les coups choisis ne soient pas totalement hasardeux, mais pas non plus trop déterministes: lorsque le coefficient de branchement est assez faible, les coups sont choisis par minimax.

Minimax intervient aussi dans les phases d'expansion et de sélection, de telle sorte que les noeuds choisis bénéficient eux aussi des résultats de minimax.


## Pour le jeu Dodo
Pour calculer les coups à étendre, nous utilisons aussi des heuristiques basées sur la connaissance du jeu: en général, lorsque un coup est joué, il est préférable de minimiser notre *score de mobilité* et de maximiser celui de l'adversaire.

De même, nous avons introduit un *score de blocage* qui reflète la capacité d'une action d'un joueur à réduire le nombre de coups légaux pour le tour suivant du joueur ennemie.

Ces heuristiques sont inspirées des travaux présentés dans [4] et [5], notamment sur la détermination des noeuds à étendre, à sélectionner ainsi que sur le déroulement des simulations.

Pour plus de détails sur le calcul de ces scores, voir le fichier source [rules.py](../game/rules.py).

Nous avons cependant ajouté un calcul des récompenses intelligent pour attirer l'exploration de l'arbre vers les noeuds qui maximisent le score de mobilité de l'adversaire tout en minimisant le sien.

Nous désignerons par $PM(g)$, $EM(g)$, et $EB(g)$ les scores de mobilité du joueur, de l'ennemi ainsi que le score de blocage de l'ennemi pour une grille donnée $g$.

Alors, la récompense $R$ issue d'une simulation dont la grille résultante est $g$ est la suivante:

$$
R(g, s) = \begin{cases}
EM(g) - PM(g) - EB(g) & \text{si } s = 0 \\
-PM(g) & \text{si } s = -1 \text{ et } PM(g) > 0 \\
-\alpha & \text{si } s = -1 \text{ et } PM(g) = 0 \\
EM(g) & \text{si } s = 1 \text{ et } EM(g) > 0 \\
\beta & \text{si } s = 1 \text{ et } EM(g) = 0 \\
\end{cases}
$$

Où $\alpha$ et $\beta$ sont des constantes que nous avons fixé expérimentalement à $\alpha = \beta = 8$.

## Pour le jeu Gopher

Pour le jeu de Gopher, nou n'avons pas implémenté d'heuristiques particulières pour la phase de rollout ou d'expansion.

## La sélection des meilleurs paramètres

Pour s'assurer des meilleurs résultats possible, une phase "d'entraînement" préalable est nécessaire pour sélectionner les meilleurs paramètres de l'exploration de l'arbre.

<div align='center'>
    <img src="videos/mcts_alpha_dodo.gif" width="280px">
</div>

Les paramètres que nous avons fait varier sont:

- La fonction de calcul du score l'UCB1, en utilisant soit la version classique, soit la version *tuned* donnée dans [3]
- Le poids d'exploration `C` de l'UCB1
- Le niveau des simulations de minimax à l'intérieur des simulations du MCTS comme décrit dans [5] pour voir l'impact du niveau des minimax sur les performances de l'algorithme.

Pour la sélection, nous avons utilisé un MCTS avec un nombre de simulations de $256$, que nous appellerons "baseline". Ce MCTS a des paramètres initiaux fixés à $C=0.7$ et la fonction UCB1 normale.

<div align='center'>
    <img src="images/mcts_stats.png" width="350px">
</div>

# Références

- [1] https://gist.github.com/qpwo/c538c6f73727e254fdc7fab81024f6e1
- [2] https://ai-boson.github.io/mcts/
- [3] https://www.lri.fr/~sebag/Slides/InvitedTutorial_CP12.pdf
- [4] Chaslot, Guillaume & Winands, Mark & Herik, H. & Uiterwijk, Jos & Bouzy, Bruno. (2008).
Progressive Strategies for Monte-Carlo Tree Search.
New Mathematics and Natural Computation. 04. 343-357. 10.1142/S1793005708001094.
- [5] H. Baier and M. H. M. Winands, "Monte-Carlo Tree Search and minimax hybrids,"
2013 IEEE Conference on Computational Inteligence in Games (CIG),
Niagara Falls, ON, Canada, 2013, pp. 1-8, doi: 10.1109/CIG.2013.6633630.
