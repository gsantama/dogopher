<div align='center'>

<img src=https://gitlab.utc.fr/uploads/-/system/project/avatar/14278/dogopher.png alt="logo" width=90 height=90 />

<h1>Dogopher</h1>
<p>Document de rendu du projet final d'IA02</p>
<p><em>Gabriel Santamaria</em><br><em>Chloé Taurel</em></p>

<a href="MCTS.md">MCTS</a> • <a href="../README.md">README</a> • <a href="HASHING.md">HASHING</a>
<br/>
</div>

<div align='center'>
    <img src="videos/mcts_alpha_gopher_128.gif" width="380px">
</div>

## Introduction

Dans ce projet, nous présentons une implémentation des jeux de plateau Dodo et Gopher, ainsi que des stratégies automatiques pour les jouer. Nous avons implémenté plusieurs stratégies différentes et avons expérimenté plusieurs paramètres pour essayer de tirer le meilleurs parti de chacune d'entre elles.

*Groupe de projet composé de: [Chloé Taurel](https://gitlab.utc.fr/taurelch) et [Gabriel Santamaria](https://gitlab.utc.fr/gsantama).*

## Table des matières

- [Introduction](#introduction)
- [Table des matières](#table-des-matières)
- [Architecture du projet](#architecture-du-projet)
  - [L'implémentation de la grille hexagonale](#limplémentation-de-la-grille-hexagonale)
  - [Les stratégies](#les-stratégies)
    - [Une stratégie ? Des stratégies !](#une-stratégie--des-stratégies-)
  - [Les règles](#les-règles)
- [Les plus... et les moins](#les-plus-et-les-moins)
  - [Les plus](#les-plus)
  - [Les moins](#les-moins)
- [Guide d'utilisation](#guide-dutilisation)
  - [Installation des dépendances](#installation-des-dépendances)
  - [Explication brève du code](#explication-brève-du-code)
    - [Grille hexagonale](#grille-hexagonale)
    - [Choix du jeu](#choix-du-jeu)
    - [Initialisation des stratégies pour chaque joueur](#initialisation-des-stratégies-pour-chaque-joueur)
    - [Créer une boucle de jeu](#créer-une-boucle-de-jeu)
  - [Bon, je veux tester moi !](#bon-je-veux-tester-moi-)
    - [Mais ? Je veux voir une game moi !](#mais--je-veux-voir-une-game-moi-)


> [!WARNING]
> **NE MANQUEZ PAS** les explications sur l'implémentation de l'algorithme MCTS qui se trouvent dans le fichier [MCTS.md](MCTS.md).


> [!WARNING]
> **NE MANQUEZ PAS** les explications sur l'optimisation du hashing de la grille se trouvent dans le fichier [HASHING.md](HASHING.md).

## Architecture du projet

Le projet est séparé en plusieurs modules qui sont les suivants:

- `game`: contient les classes qui implémentent les règles de jeux ainsi que les stratégies utilisées par nos IA.
- `helpers`: contient des fonctions utilitaires pour les différentes stratégies implémentées, notamment pour ce qui est de la stratégie `MCTS`: vous pourrez y trouver notamment une implémentation de l'arbre de recherche utilisé par l'algorithme.
- `hexgrid`: contient notre implémentation d'une grille hexagonale, utilisée pour représenter le plateau de jeu. 

### L'implémentation de la grille hexagonale

Nous avons utilisé les contraintes fournies par le sujet du projet sur la page Moodle. La grille est représentée par une matrice `Numpy`. L'utilisation de `Numpy` ici n'est pas anodine, puisque cela nous permet par la suite d'utiliser des fonctions vectorisées pour effectuer tout un tas de calculs (notamment celui des coups légaux).

La classe `HexGrid` fait $599$ lignes de codes et contient tout un tas de méthodes utilitaires et qui ne sont pas forcément liées directement à la compétition, mais qui ont été utilisées pour le développement de nos algorithmes et le débuggage de notre code (cf. la fonction `debug_plot` par exemple).

### Les stratégies

Nous avons expérimenté $3$ stratégies différentes pour ce projet:

- `MCTS`: une implémentation de l'algorithme "Monte-Carlo Tree-Search"
- `AlphaBeta`: une implémentation de l'algorithme de recherche minimax avec élagage alpha-beta.
- `Negascout`: une modification de minimax appelée `Negascout`

#### Une stratégie ? Des stratégies !

Toutes les stratégies implémentées héritent de la classe `Strategy` qui définit les méthodes que chaque stratégie doit implémenter.

Vous trouverez les différentes implémentations des stratégies dans le module `game/strategy`.

### Les règles

De même que pour les stratégies, les règles ont été implémentées à partir d'une classe abstraite `Rules` qui définit les méthodes que chaque "règle de jeu" doit implémenter.

Vous trouverez les différentes implémentations des règles dans le module `game/rules`.

## Les plus... et les moins

D'une manière générale, le projet est assez facile à maintenir: nous n'avons par exemple pas touché au code de la grille hexagonale depuis un petit bout de temps. De même, outre certaines optimisations, la logique des règles des jeux Dodo et Gopher n'a pas été touchée depuis que nous l'avons implémentée vers le début du projet.

### Les plus

- Il est très facile d'ajouter de nouvelles stratégies ou de nouveaux jeux: il suffit d'ajouter une nouvelle classe qui hérite de `Strategy` ou d'implémenter de nouvelles règles en héritant de `Rules`.
- L'implémentation grille hexagonale est assez optimisée et très facile à utiliser.
- L'algorithme MCTS utilise une recherche en parralèle, et c'est cool (en plus de permettre de faire tourner $4096$ simulations en moins de $15$ secondes sur nos machines).
- Plusieurs stratégies sont implémentées, nous avons même essayé d'ajouter une "quiescence search" à `Negascout`, ce qui a augmenté ses performances 


### Les moins

- Les poids pour le MCTS ne sont pas bons: nous n'avons pas eu le temps d'effectuer assez de calculs pour trouver les bons poids (nous avons essayé d'utiliser des algorithmes génétiques pour le faire, mais faute de temps nous n'avons pu le faire que pour $5$ générations de $5$ individus sur $5$ games à chaque "combat").
- Certaines parties du code sont obsolètes et n'ont pas été nettoyées par manque de temps (ex: la fonction `filter` de la `HexGrid`).
- Le code du client a été "hotfix" pendant la compétition, ce qui nous a empêché de faire les premiers warmups.

## Guide d'utilisation

### Installation des dépendances

Pour installer les dépendances du projet, vous pouvez utiliser le fichier `requirements.txt` fourni. Pour ce faire, vous pouvez utiliser la commande suivante:

```bash
pip install -r requirements.txt
```

### Explication brève du code

#### Grille hexagonale

La grille hexagonale a plusieurs fonctions d'initialisation.

Pour jouer en mode `Gopher`, on peut initialiser la grille comme suit:

```python
grid = HexGrid.empty_state(size)
```
Pour jouer en mode `Dodo`, on doit ajouter en arguments la valeur entière (représentant un joueur) à utiliser pour remplir la partie supérieure de la grille, ainsi que la valeur entière à utiliser pour la partie inférieure de la grille. 
On peut initialiser la grille comme suit:

```python
grid = HexGrid.split_state(size, top_player, botom_player)
```

#### Choix du jeu

Le choix du jeu se fait en créant une instance de la classe Rules, qui déterminera si l'on joue à `Dodo` ou à `Gopher`

```python
rules = Dodo()
rules = Gopher()
```

#### Initialisation des stratégies pour chaque joueur

Nous initialisons nos joueurs avec une instance de la stratégie qu'ils utilisent. Une stratégie prend en argument la grille sur laquelle elle va jouer, les règles (c'est à dire le jeu) qu'elle doit suivre, et la valeur du joueur pour lequel elle joue. Voici comment s'y prendre :

```python
player_one = StrategyAlphaBeta(grid, rules, 1, depth=15)
player_two = MCTSStrategy(grid, rules, 2)
```
*(Notons que pour la stratégie minmax AlphaBeta, on peut éventuellement ajouter une valeur à la profondeur de recherche, qui est par défaut à 3).*

NB : Nous avons aussi implémenté une `BrainStrategy`, qui vous permet de jouer contre une IA en rentrant les coups au clavier. Elle fonctionne comme les autres:

```python
player_one = BrainStrategy(grid, rules, 1)
```

#### Créer une boucle de jeu

Vous pouvez voir dans les fichiers strategy.py et rules.py les méthodes que nous utilisons pour dérouler une partie. Voici un exemple de boucle de jeu simple *(ici entre minmax et mcts en suivant les exemples d'initialisation précédents)*:

```python
ply = 1
while not rules.game_over(grid):
    if ply == 1:
        action = player_one.get_action(grid, ply)
        grid.move(action, ply)
    else:
        action = player_two.get_action(grid, ply)
        grid.move(action, ply)
    ply = other(ply)

ply_one_won = rules.has_won(grid, 1)
if ply_one_won:
    print("Minimax won")
else:
    print("MCTS won")
```

### Bon, je veux tester moi !

Nous vous avons préparé un script de test dans le fichier `test.py`. Vous pouvez lancer le progamme, choisir les stratégies que vous voulez faire s'affronter.

```bash
python3 test.py # Après avoir installé toutes les dépendances
```

Vous devrez utiliser votre clavier pour sélectionner les options et les configuration de jeu.

#### Mais ? Je veux voir une game moi !
Pour voir les résultats, finaux ainsi que la game, vous pouvez utiliser le script `gen.sh` qui génère un fichier `.gif` ou `.mp4` selon ce que vous avez choisi. Voici comment l'utiliser:

```bash
# Placez vous à la racine du projet
cd plots/
./gen.sh -gif -o game # ou ./gen.sh -mp4
```

Un fichier `game.gif` ou `game.mp4` sera généré dans le dossier `plots/` et vous pourrez voir la partie qui a été jouée.
