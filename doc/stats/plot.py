import pandas as pd
from matplotlib import pyplot as plt

df = pd.read_csv("doc/stats/mcts.csv")

df["Win_rate"] = df["Wins"]
plt.figure(figsize=(12, 6))
for ucb in df["UCB"].unique():
    subset = df[df["UCB"] == ucb]
    ucb_str = "UCB"
    if ucb == 1.0:
        ucb_str = "UCB Tuned"
    plt.plot(subset["Weight"], subset["Win_rate"], marker="o", label=f"{ucb_str}")

plt.xlabel("Weight")
plt.ylabel("Win Rate (%)")
plt.title("Win Rate (%) vs Weight for different UCB values")
plt.legend()
plt.grid(True)
plt.show()
