"""
Hexagonal grids utility functions and classes definitions.
Entry point for the hexgrid module.
"""

from typing import Generator, Callable

import numpy as np
from matplotlib import use
import matplotlib.pyplot as plt
from matplotlib.patches import RegularPolygon
import matplotlib.lines as mlines
from xxhash import xxh32_intdigest as xxh

from api import State, Cell, Player, Action
from game.utils import other

# This fixes a little bug when using the MCTS strategy
# where GTK cries because, quotation "we're not in the main loop"
# This is due to the launch of a lot of processes in parallel
# Agg is fine, just use the gen.sh generator to get a gif or a mp4
# of the game at the end
use("Agg")


class HexGrid:
    """
    A HexGrid is an hexagonal grid represented as a 2D array of tiles
    """

    Plots = 0

    # This is a static cache of the neighbours. Then, if we have
    # to reconstruct the HexGrid each time the opponent made a move,
    # we don't have to recompute the neighbours of each cell
    # Btw, would be way much better if we could know what move the
    # opponent did :p
    Neighbours_cache: dict[Cell, tuple[list[Cell], list[Cell]]] = {}

    # We cache the is_empty result to avoid recomputing it
    # Since we don't know how the computing time will
    # be taken in consideration :p saving where we can kek
    __is_empty: bool = True
    __last_played: Player = 0  # Last player to have moved a cell
    # We also cache the number of checkers on the grid for each player
    __ncheckers: list[int] = [0, 0, 0]

    size: tuple[int, int]
    grid: np.ndarray[Player]

    def __init__(self, state: State, size: int):
        """
        Create a new HexGrid of a given size with all tiles initialized to the same value.
        """
        self.size = (size, 2 * size - 1)
        self.grid = self.__array_of_state(state)

        if not HexGrid.Neighbours_cache:
            HexGrid.Neighbours_cache = self.__precompute_neighbours()

    def __precompute_neighbours(self):
        neighbours_table = {}
        for cell, _ in self.iter():
            neighbours_table[cell] = self.__get_neighbours(cell)
        return neighbours_table

    def __get_neighbours(self, cell) -> tuple[list[Cell], list[Cell]]:
        x, y = cell

        x_min, y_min = -self.size[0], -self.size[0]
        x_max, y_max = self.size[0], self.size[0]

        north: list[Cell] = [(x, y + 1), (x + 1, y), (x + 1, y + 1)]
        south: list[Cell] = [(x, y - 1), (x - 1, y), (x - 1, y - 1)]

        north = [
            cell
            for cell in north
            if x_min < cell[0] < x_max and y_min < cell[1] < y_max
        ]
        south = [
            cell
            for cell in south
            if x_min < cell[0] < x_max and y_min < cell[1] < y_max
        ]

        north: np.ndarray = list(north)
        south: np.ndarray = list(south)

        return north, south

    def __array_of_state(self, state: State) -> np.ndarray:
        """
        Convert a State (representing a HexGrid) to a flattened version of it
        (this is a private method)

        Args:
            state: The state to convert
        """

        if not state:
            return np.zeros(0, dtype=np.int8)

        # We initially have all the cells in a list with
        # a shape of ((x, y), player)
        zipper: list = list(
            zip(*state)
        )  # this transform the list into [((x, y), (x, y), ...), (player, player, ...)]
        cells: list[Cell] = list(zipper[0])
        players: list[Player] = list(zipper[1])

        width: int = self.size[1]
        height: int = self.size[1]

        array = -np.ones((width, height), dtype=np.int8)

        # To store everything inside a 2D Array, we just have to reverse the index
        # Cell(q, r) is at position [r][q] inside our array
        # @see https://www.redblobgames.com/grids/hexagons/#map-storage
        cells = [self.__hex_to_flat(cell) for cell in cells]

        for key, cell in enumerate(cells):
            if players[key] != 0:
                self.__is_empty = False

            ply = players[key]
            array[cell[0]][cell[1]] = ply
            self.__ncheckers[ply] += 1

        # To make the hashing run faster, we directly convert to a contiguous array
        return np.ascontiguousarray(array)

    def __state_of_array(self) -> State:
        """
        Convert a flattened HexGrid to a State (this is a private method)

        Args:
            array: The array to convert
        """

        state: State = []
        for cell, player in self.iter():
            state.append((cell, player))

        return state

    def get_state(self) -> State:
        """
        Get the state of the grid as a State.

        Returns:
            The state of the grid
        """
        return self.__state_of_array()

    @property
    def last_played(self) -> Player:
        """
        Returns the ID of the last player that moved something.
        """
        return self.__last_played

    @last_played.setter
    def last_played(self, ply: Player) -> None:
        """
        Set the last player that moved something.

        Args:
            ply: The player that moved something
        """
        self.__last_played = ply

    @property
    def ncheckers(self) -> list[int]:
        """
        Returns the amount of checkers for each player on the board.
        """
        return self.__ncheckers

    @ncheckers.setter
    def ncheckers(self, ncheckers: list[int]) -> None:
        """
        Set the amount of checkers for each player on the board.

        Args:
            ncheckers: The amount of checkers for each player
        """
        self.__ncheckers = ncheckers

    @property
    def is_empty(self) -> bool:
        """
        Returns whether the grid is empty or not.
        """
        return self.__is_empty

    @is_empty.setter
    def is_empty(self, is_empty: bool) -> None:
        """
        Set whether the grid is empty or not.

        Args:
            is_empty: True if the grid is empty, False otherwise
        """
        self.__is_empty = is_empty

    @property
    def neighbours(self) -> dict[Cell, tuple[list[Cell], list[Cell]]]:
        """
        Returns the neighbours of each cell in the grid.
        """
        return HexGrid.Neighbours_cache

    @neighbours.setter
    def neighbours(self, neighbours: dict[Cell, tuple[list[Cell], list[Cell]]]) -> None:
        """
        Set the neighbours of each cell in the grid.

        Args:
            neighbours: The neighbours of each cell in the grid
        """
        HexGrid.Neighbours_cache = neighbours

    def get_number_checkers(self, ply: Player) -> int:
        """
        Returns the amount of checkers a player have on the board.

        Args:
            ply: The player

        Returns:
            The amount of checkers a player have on the board
        """
        return self.__ncheckers[ply]

    def __hex_to_flat(self, cell: Cell) -> Cell:
        """
        Convert hexagonal cell coordonates to flat cell coordonates.

        Args:
            cell: The cell to normalize

        Returns:
            The converted cell
        """

        size = self.size[0]
        q, r = cell
        r = -r
        x = size - r - 1
        y = size - q - 1
        return (y, x)

    def __getitem__(self, key: Cell) -> Player:
        """
        Get the tile at the given coordinates.
        """
        key = self.__hex_to_flat((key[0], key[1]))
        return self.grid[key[0]][key[1]]

    def __setitem__(self, key: Cell, ply: Player) -> None:
        """
        Set the tile at the given coordinates to a specific player.
        """
        key = self.__hex_to_flat((key[0], key[1]))
        self.grid[key[0]][key[1]] = ply

        if ply != 0:
            self.__is_empty = False

    def move(self, action: Action, ply: Player) -> None:
        """
        Move a tile from one cell to another.
        """
        # We need to separate the case whre it's a Dodo action
        # from the one when it's a Gopher action.
        match action:
            case (
                (_, _) as f,
                (_, _) as t,
            ):  # We're playing Dodo game, actions type is ActionDodo
                if not self[f] == ply:
                    raise ValueError(
                        f"[HexGrid] Invalid move: {f} -> {t} of player {ply}. {f} belong to player {self[f]}."
                    )

                if not self[t] == 0:
                    raise ValueError(
                        f"[HexGrid] Invalid move: {f} -> {t} of player {ply}. {t} is already occupied by {self[t]}."
                    )

                self[t] = self[f]
                self[f] = 0
            case (
                _,
                _,
            ) as t:  # We're playing Gopher game and actions type is ActionGopher :p
                if not self[t] == 0:
                    raise ValueError(
                        f"[HexGrid] Invalid move: {f} -> {t} for player {ply}. {t} is already occupied by {self[t]}."
                    )

                self[t] = ply
                self.__ncheckers[ply] += 1

        self.__last_played = ply

    def undo(self, action: Action) -> None:
        """
        Undo a move.

        Args:
            action: The action to undo.

        Warning: this does not perform any check on the fact
        that the action has been already done or not.
        """

        match action:
            case (
                (_, _) as f,
                (_, _) as t,
            ):
                ply = self[t]
                action = (t, f)
                self.move(action, ply)
            case (_, _) as t:
                self[t] = 0

        self.__last_played = other(self.__last_played)

    def to_play(self):
        """
        Returns the ID of the next player that have to play a move.
        """
        return other(self.__last_played)

    def __deepcopy__(self, memo=None) -> "HexGrid":
        """
        Create a deep copy of the grid.
        """
        # We copy the numpy array following the C layout
        narr = np.copy(self.grid)
        ngrid = HexGrid([], 0)

        ngrid.grid = narr
        ngrid.size = self.size
        ngrid.is_empty = self.is_empty
        ngrid.last_played = self.last_played
        ngrid.ncheckers = self.ncheckers

        return ngrid

    def __str__(self) -> str:
        """
        Pretty-prints the grid as a string.
        """
        cell_size = 5
        nrows, ncols = self.grid.shape

        title = "2D Shape:\n"
        spaces = " " * (((ncols * cell_size) - len(title) + 3) // 2)

        res: str = spaces + title + "-" * (ncols * cell_size + 2) + "\n"
        for i in range(0, nrows * cell_size, cell_size):
            res += "|"
            for j in range(0, ncols * cell_size, cell_size):
                cell = self.grid[i // cell_size, j // cell_size]
                res += f"{cell:^{cell_size}}"
            res += "|\n"

        res += "-" * (ncols * cell_size + 2)
        return str(res)

    def __eq__(self, o):
        return isinstance(o, HexGrid) and np.array_equal(self.grid, o.grid)

    def __hash__(self) -> int:
        # Fast hashing of the grid (which is the state of the game btw)
        # See: https://stackoverflow.com/a/75870138
        return xxh(self.grid)

    def iter(self) -> Generator[tuple[Cell, Player], None, None]:
        """
        Iterate over the hexagonal grid.

        Returns:
            A generator of the hexagonal grid.

        """
        size = self.size[0]
        limit = size - 1

        for x in range(-limit, size):
            for y in range(-limit, size):
                if self[x, y] == -1:
                    continue
                yield (x, y), self[x, y]

    def filter(self, f: Callable[[Cell, Player], bool]) -> list[Cell]:
        """
        Filter the hexagonal grid using the filter function given as a parameter.
        The result is a sub set of the cells where, for each cell c, f(c) is True.

        Args:
            f: Callable[[Cell, Player], bool]: The filter function to apply.

        Returns:
            A list of cells.
        """
        result = []
        for cell, player in self.iter():
            if f(cell, player):
                result.append(cell)
        return result

    def get_cells(self, ply: Player) -> np.ndarray[np.int8]:
        """
        Get the cells of a player.

        This is a faster way of getting the cells that belongs to
        a player than using the filter method. It uses numpy arrays
        to vectorize the operations.

        Args:
            ply: The player to get the cells of.

        Returns:
            A stack array representing the cells that the player owns
        """
        cells = np.column_stack(np.where(self.grid == ply))

        # We need then to convert back this
        # into the hexagonal coordinates
        # since the __flat_to_hex function isn't vectorized,
        # we do this manually
        size = self.size[0] - 1
        x = cells[:, 0] - size
        y = cells[:, 1] - size
        flat = (-x, y)
        return np.column_stack(flat)

    def get_neighbours(self, cell: Cell, direction: int = -1) -> list[Cell]:
        """
        Get the neighbours of a cell.
        Uses the cached neighbours for fast `O(1)` access.
        This assumes the neighbours have already been cached.

        Args:
            cell: The cell to get the neighbours of.
            direction: The direction to get the neighbours of. Optionnal parameter.

        Returns:
            The list of neighbours.
        """
        cneighbours: list

        match direction:
            case -1:  # We're getting the neighbours in all directions
                cneighbours = self.neighbours[cell][0] + self.neighbours[cell][1]
            case 0:
                cneighbours = self.neighbours[tuple(cell)][0]
            case 1:
                cneighbours = self.neighbours[tuple(cell)][1]

        return cneighbours

    @classmethod
    def empty_state(cls, size: int):
        """
        Generate an empty state for a given size.

        Details about computation: https://www.redblobgames.com/grids/hexagons/

        Args:
            size: The size of the grid.

        Returns:
            An instance of HexGrid with an empty state.
        """
        size = size - 1
        coordinates = []
        for r in range(-size, size + 1):
            for q in range(max(-size, r - size), min(size, r + size) + 1):
                coordinates.append(((q, r), 0))

        return cls(coordinates, size + 1)

    @classmethod
    def split_state(cls, size: int, top: Player, bot: Player):
        """
        Split the hex grid into two parts and fill them with given integers.

        Details about computation: https://www.redblobgames.com/grids/hexagons/

        Args:
            size: The size of the grid.
            top: The value to fill the top half of the grid.
            bot: The value to fill the bottom half of the grid.

        Returns:
            The state of the grid and its size.
        """
        coordinates = []

        # Band is the size of the middle band of the grid,
        # separating each half of the grid
        band = (
            size // 3
        )  # we fine tuned this to get the exact result when using a grid of size 7 (band = 1)

        size = size - 1
        coordinates = []
        for r in range(-size, size + 1):
            for q in range(max(-size, r - size), min(size, r + size) + 1):
                rpq = abs(r + q)
                in_band = rpq <= band

                if in_band:  # are we in the band (rock'n'roll mf) ?
                    coordinates.append(((q, r), 0))
                elif r + q >= 0:
                    coordinates.append(((q, r), top))
                else:
                    coordinates.append(((q, r), bot))

        return cls(coordinates, size + 1)

    @classmethod
    def random_state(cls, size: int):
        """
        Generate a random hex grid for a given size.

        Args:
            size: The size of the grid

        Returns:
            A random hex grid
        """
        size = size - 1
        coordinates = []
        for r in range(-size, size + 1):
            for q in range(max(-size, r - size), min(size, r + size) + 1):
                coordinates.append(((q, r), np.random.randint(0, 3)))

        return cls(coordinates, size + 1)

    @staticmethod
    def __get_color(ply: Player) -> str:
        color = "#faebd7"  # a nice beige
        match ply:
            case 1:
                color = "red"
            case 2:
                color = "blue"
            case 3:
                color = "purple"

        return color

    @staticmethod
    def __add_hexagon(ax, x_center, y_center, q, r, player=0, **kwargs):
        color = HexGrid.__get_color(player)

        hexagon = RegularPolygon(
            (x_center, y_center),
            numVertices=6,
            radius=4 / (np.sqrt(3) * 5),
            orientation=np.radians(30),
            facecolor=color,
            **kwargs,
        )

        ax.add_patch(hexagon)
        # Add text annotation for the hex coordinates (q, r)
        ax.text(
            x_center,
            y_center,
            f"({q},{r})",
            ha="center",
            va="center",
            fontsize=8,
            color="black",
        )

    def debug_plot(self, bold=None, save=False, labels=None):
        """
        Plot the hex grid in a matplotlib window.

        This is a debug function to control the state of the grid.

        Note: the display of the hexagonal grid is not perfect and
        might be a bit disrupting but it's fine :p

        Args:
            size: The size of the hexagons.
        """
        if bold is None:
            bold = []

        _, ax = plt.subplots()

        for (q, r), player in self.iter():
            if (q, r) in bold:
                player = 3

            x = q * np.sin(np.radians(45)) - r * np.cos(np.radians(45))
            y = (q * np.cos(np.radians(45)) + r * np.sin(np.radians(45))) * 9 / 16
            HexGrid.__add_hexagon(ax, x, y, q, r, player=player, edgecolor="black")

        ax.set_aspect("equal")
        ax.axis("off")
        plt.autoscale(enable=True)

        if labels is not None:
            legend = []
            for label, player in labels:
                color = HexGrid.__get_color(player)
                line = mlines.Line2D(
                    [], [], color=color, marker="o", markersize=10, label=label
                )
                legend.append(line)

            ax.legend(handles=legend, loc="upper right", bbox_to_anchor=(1.3, 1.12))

        if save:
            plt.savefig(f"plots/hexgrid_{HexGrid.Plots}.png")
            plt.close()
            HexGrid.Plots += 1
            return

        plt.show()
