"""
Dogopher: A Gopher and Dodo AI (IA02 P24 Project)
"""

import argparse

from typing import Dict, Any
from gndclient import start, Action, Score, Player, State, Time, DODO_STR, GOPHER_STR
from hexgrid.grid import HexGrid
from game.rules import Rules, Dodo, Gopher
from game.strategy import Strategy, MCTSStrategy

Environment = Environment = Dict[str, Any]

strategy_mcts = None
ggrid = None
grules = None
gsize = None


def initialize(
    game: str, state: State, player: Player, hex_size: int, _: Time
) -> Environment:
    """
    Initialize the game environment.

    Args:
        game: The name of the game.
        state: The initial state of the game.
        player: The player number.
        hex_size: The size of the hexagons.
        total_time: The total time for the game.

    Returns:
        The game environment.

    API from: https://gitlab.utc.fr/ia02/gopher-and-dodo

    """
    print("Initializing the game environment.")
    print("Playing", game, "as player", player)
    rules: Rules = Gopher() if game == GOPHER_STR else Dodo()
    grid: HexGrid = HexGrid(state, hex_size)

    global strategy_mcts
    global ggrid
    global grules
    global gsize

    ggrid = grid
    grules = rules

    print("Using hex size:", hex_size)

    strat: Strategy = None

    nsim = 512 if game == GOPHER_STR else 2048

    strat = MCTSStrategy(grid, rules, player, simulations=nsim)

    strategy_mcts = strat

    gsize = hex_size

    return {}


def strategy(env: Environment, state: State, player: Player, time_left: Time) -> Action:
    """
    The strategy of the player. Launched every time it's the turn of our AI to play.

    Args:
        env: The game environment.
        state: The current state of the game.
        player: The player number.
        time_left: The time left for the player.

    Returns:
        The updated game environment and the action to play.

    API from: https://gitlab.utc.fr/ia02/gopher-and-dodo

    """
    hex_size: int = gsize
    strat: Strategy = strategy_mcts

    grid = HexGrid(state, hex_size)

    action = strat.get_action(grid, player, time_left=time_left)

    print("Playing action:", action, "as player", player)

    if isinstance(grules, Dodo):
        return (env, action)
    else:
        return (env, (action,))


def final(_: State, score: Score, player: Player):
    """
    Callback of when the game is finished.

    Args:
        state: The final state of the game.
        score: The final score of the game.
        player: The player number.
    """
    print(f"Ending: {player} wins with a score of {score}.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="ClientTesting", description="Test the IA02 python client"
    )

    parser.add_argument("group_id")
    parser.add_argument("members")
    parser.add_argument("password")
    parser.add_argument("-s", "--server-url", default="http://localhost:8080/")
    parser.add_argument("-d", "--disable-dodo", action="store_true")
    parser.add_argument("-g", "--disable-gopher", action="store_true")
    args = parser.parse_args()

    available_games = [DODO_STR, GOPHER_STR]
    if args.disable_dodo:
        available_games.remove(DODO_STR)
    if args.disable_gopher:
        available_games.remove(GOPHER_STR)

    start(
        args.server_url,
        args.group_id,
        args.members,
        args.password,
        available_games,
        initialize,
        strategy,
        final,
        gui=True,
    )
