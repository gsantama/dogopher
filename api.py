"""
This module contains the type definitions for the API of the game.
API from: https://gitlab.utc.fr/ia02/gopher-and-dodo
"""

from typing import Union

Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]
Action = Union[ActionGopher, ActionDodo]
Player = int
State = list[tuple[Cell, Player]]
Score = int
Time = int
