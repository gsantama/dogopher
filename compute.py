"""
Little helper module to try to find the best parameters of
the MCTS algorithm.

Shitty code, just to make it run and get the data
"""

import gc
import time
import os
from copy import deepcopy
from random import choice, random
import numpy as np
from tqdm import tqdm
from deap import base, creator, tools, algorithms

from hexgrid.grid import HexGrid
from game.rules import Rules, Dodo
from game.strategy import MCTSStrategy, StrategyAlphaBeta
from game.helpers.mcts import UCB
from game.utils import other

NITER_PER_GAME = 2


def legals_bench(n=1000):
    """
    Needed some benchmarking of the get_legal_moves function
    """
    ttime = 0
    for _ in range(n):
        grid: HexGrid = HexGrid.random_state(6)
        rules: Rules = Dodo()
        ply = choice([1, 2])
        s = time.time()
        _ = rules.get_legal_moves(grid, ply)
        ttime += time.time() - s

    print(f"Time: {ttime / n:.10f}")


# Parameters to choose from
# The ultimate goal is to find the perfect couple
# that will make MCTS win most of the time.
ucb = [UCB.ucb, UCB.ucb_tuned]
weights = np.arange(0.5, 10, 1)
levels = [10]


def game(one: MCTSStrategy, two: MCTSStrategy, grid: HexGrid, rules: Rules):
    """
    Run a single game with the given players
    """

    grid: HexGrid = deepcopy(grid)

    ply = 1
    while not rules.game_over(grid):
        if ply == 1:
            action = one.get_action(grid, ply)
            grid.move(action, ply)
        else:
            action = two.get_action(grid, ply)
            grid.move(action, ply)

        ply = other(ply)

    if rules.has_won(grid, 1):
        return 1  # First player won
    else:
        return 2  # Second player won


def evaluate(individuals):
    scores = {i: 0 for i in range(len(individuals))}
    num_games = NITER_PER_GAME

    with tqdm(total=len(individuals) * len(individuals) * num_games) as pbar:
        for i, _ in enumerate(individuals):
            for j in range(i + 1, len(individuals)):
                for _ in range(num_games):
                    grid: HexGrid = HexGrid.split_state(4, 2, 1)
                    rules: Rules = Dodo()

                    mcts1 = MCTSStrategy(
                        grid,
                        rules,
                        ply=1,
                        simulations=256,
                        exploration_weight=individuals[i][0],
                        threshold=individuals[i][1],
                        ucb=individuals[i][2],
                    )
                    mcts2 = MCTSStrategy(
                        grid,
                        rules,
                        ply=2,
                        simulations=256,
                        exploration_weight=individuals[j][0],
                        threshold=individuals[j][1],
                        ucb=individuals[j][2],
                    )

                    try:
                        result = game(mcts1, mcts2, grid, rules)
                    except ValueError:
                        tqdm.write(
                            "[ValueError] with parameters %s and %s"
                            % (individuals[i], individuals[j])
                        )
                        tqdm.write("Skipping 1 game...")

                        gc.collect()

                        pbar.update(1)
                        continue
                    except RuntimeError:
                        tqdm.write(
                            "[RuntimeError] with parameters %s and %s"
                            % (individuals[i], individuals[j])
                        )
                        tqdm.write("Skipping 1 game...")

                        gc.collect()

                        pbar.update(1)
                        continue

                    if result == 1:
                        scores[i] += 1
                    elif result == 2:
                        scores[j] += 1

                    pbar.update(1)
                    gc.collect()

    for i, _ in enumerate(individuals):
        individuals[i].fitness.values = (scores[i],)


def custom_mutate(individual, indpb=0.1):
    for i in range(len(individual)):
        if random() < indpb:
            if i == 0:  # exploration_weight
                individual[i] = np.random.uniform(0, 2.0)
            elif i == 1:  # threshold
                individual[i] = np.random.randint(10, 50)
            elif i == 2:  # ucb
                individual[i] = np.random.choice([UCB.ucb, UCB.ucb_tuned])
    return (individual,)


seed = np.random.SeedSequence(int.from_bytes(os.urandom(16), "big") * os.getpid())
rng = np.random.Generator(np.random.MT19937(seed))

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("attr_exploration_weight", np.random.uniform, 0, 2.0)
toolbox.register("attr_threshold", np.random.randint, 10, 50)
toolbox.register("attr_ucb", np.random.choice, [UCB.ucb, UCB.ucb_tuned])

toolbox.register(
    "individual",
    tools.initCycle,
    creator.Individual,
    (toolbox.attr_exploration_weight, toolbox.attr_threshold, toolbox.attr_ucb),
    n=1,
)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", custom_mutate, indpb=0.1)
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("evaluate", evaluate)

population = toolbox.population(n=5)
ngen = 3
cxpb = 0.5
mutpb = 0.2


tqdm.write("Start of genetic selection...")
for gen in range(ngen):
    tqdm.write(f"Generation number {gen}")
    toolbox.evaluate(population)
    offspring = algorithms.varAnd(population, toolbox, cxpb, mutpb)
    toolbox.evaluate(offspring)
    population = toolbox.select(offspring + population, k=len(population))

tqdm.write("End of genetic selection...")
tqdm.write("Selecting the best inividual...")
best_ind = tools.selBest(population, 1)[0]
print("Best individual is: %s\nwith fitness: %s" % (best_ind, best_ind.fitness.values))


def run_one():
    grid: HexGrid = HexGrid.split_state(4, 2, 1)
    rules: Rules = Dodo()
    rules.clear_cache()

    player_two = MCTSStrategy(grid, rules, 2)
    player_one = StrategyAlphaBeta(grid, rules, 1, depth=15)

    ply = 1

    ttimes = [0, 0]
    niter = [0, 0]
    wins = [0, 0]

    while not rules.game_over(grid):
        if ply == 1:
            s = time.time()
            try:
                action = player_one.get_action(grid, ply)
                ttimes[1] += time.time() - s
                niter[1] += 1
                grid.move(action, ply)
            except RuntimeError:
                print("Runtime error, retrying...")
                continue
        else:
            s = time.time()
            try:
                action = player_two.get_action(grid, ply)
                ttimes[0] += time.time() - s
                niter[0] += 1
                grid.move(action, ply)
            except RuntimeError:
                print("Runtime error, retrying...")
                continue

        ply = other(ply)

    ply_one_won = rules.has_won(grid, 1)
    if ply_one_won:
        tqdm.write("Minimax won")
        wins[1] += 1
    else:
        tqdm.write("MCTS won")
        wins[0] += 1

    grid.debug_plot(save=True)
    gc.collect()
    return wins, ttimes, niter


def get_stats(n=100):

    wins = [0, 0]
    ttimes = [0, 0]
    niters = [0, 0]

    for _ in tqdm(range(n)):
        w, t, n = run_one()
        wins[0] += w[0]
        wins[1] += w[1]
        ttimes[0] += t[0]
        ttimes[1] += t[1]
        niters[0] += n[0]
        niters[1] += n[1]

    return wins, ttimes, niters


# wins, ttimes, niters = get_stats(10)

# print(f"MCTS wins: {wins[0]}")
# print(f"AlphaBeta wins: {wins[1]}")
# print(f"MCTS time: {ttimes[0] / niters[0]:.10f}")
# print(f"AlphaBeta time: {ttimes[1] / niters[1]:.10f}")
