"""
Testing script for the project
"""

from hexgrid.grid import HexGrid
from game.rules import Dodo, Gopher
from game.strategy import (
    MCTSStrategy,
    StrategyAlphaBeta,
    BrainStrategy,
    RandomStrategy,
    NegascoutStrategy,
)
from game.utils import other


def test():
    """
    Main test function
    """

    print("Choisissez le jeu auquel vous voulez jouer : ")
    print("\t 1. Gopher")
    print("\t 2. Dodo")

    choix_jeu = int(input("Choix du jeu : "))
    while choix_jeu != 1 and choix_jeu != 2:
        print(f"Savez-vous lire ? Vous devez choisir 1 ou 2, pas {choix_jeu}...")
        choix_jeu = int(input("Choix du jeu : "))

    choix = int(input("Taille de la grille : "))
    while choix < 2:
        choix = int(input("Taille de la grille : "))

    if choix_jeu == 1:
        rules = Gopher()
        grid = HexGrid.empty_state(choix)
    else:
        rules = Dodo()
        grid = HexGrid.split_state(choix, 2, 1)

    str_one = None
    str_two = None

    for i in range(2):
        print(f"Choisissez la stratégie du joueur {i+1} : ")
        print("\t 1. Brain (attention c'est difficile)")
        print("\t 2. Random (la + smart)")
        print("\t 3. MinMax (avec élagage AlphaBeta)")
        print("\t 4. Negascout (avec quiescence search)")
        print("\t 5. MCTS (utilisée pour la compétition)")
        choix = int(input(f"Choix de la strategie du joueur {i+1} : "))
        while choix < 1 or choix > 5:
            print(
                "Calmez-vous, lisez doucement et regardez ! Il faut un chiffre entre 1 et 5!"
            )
            choix = int(input(f"Choix de la strategie du joueur {i+1} : "))
        if i == 0:
            if choix == 1:
                player_one = BrainStrategy(grid, rules, 1)
                str_one = "Brain"
            elif choix == 2:
                player_one = RandomStrategy(grid, rules, 1)
                str_one = "Random"
            elif choix == 3:
                player_one = StrategyAlphaBeta(grid, rules, 1, depth=15)
                str_one = "Minmax"
            elif choix == 4:
                player_one = NegascoutStrategy(grid, rules, 1, depth=15)
                str_one = "Negascout"
            else:
                player_one = MCTSStrategy(grid, rules, 1)
                str_one = "MCTS"
        else:
            if choix == 1:
                player_two = BrainStrategy(grid, rules, 2)
                str_two = "Brain"
            elif choix == 2:
                player_two = RandomStrategy(grid, rules, 2)
                str_two = "Random"
            elif choix == 3:
                player_two = StrategyAlphaBeta(grid, rules, 2, depth=15)
                str_two = "Minmax"
            elif choix == 4:
                player_two = NegascoutStrategy(grid, rules, 2, depth=15)
                str_two = "Negascout"
            else:
                player_two = MCTSStrategy(grid, rules, 2)
                str_two = "MCTS"

    ply = 1
    while not rules.game_over(grid):
        if ply == 1:
            action = player_one.get_action(grid, ply)
            grid.move(action, ply)
            print(f"Player 1 ({str_one}) played: ", action)
            grid.debug_plot(labels=[(str_one, 1), (str_two, 2)], save=True)
        else:
            action = player_two.get_action(grid, ply)
            grid.move(action, ply)
            print(f"Player 2 ({str_two}) played: ", action)
            grid.debug_plot(labels=[(str_one, 1), (str_two, 2)], save=True)
        ply = other(ply)

    ply_one_won = rules.has_won(grid, 1)
    if ply_one_won:
        print("Player 1 won")
    else:
        print("Player 2 won")

    grid.debug_plot(labels=[(str_one, 1), (str_two, 2)], save=True)


if __name__ == "__main__":
    print("***************************************************************")
    print("*                SCRIPT DE TEST DU PROJET                     *")
    print("*                         dogopher                            *")
    print("*                                                             *")
    print("*        Gabriel Santamaria & Chloé Taurel - IA02 2024        *")
    print("***************************************************************")
    test()
