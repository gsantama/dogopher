<div align='center'>

<img src=https://gitlab.utc.fr/uploads/-/system/project/avatar/14278/dogopher.png alt="logo" width=90 height=90 />

<h1>Dogopher</h1>
<p>Projet de fin de semestre de l'UV IA02</p>



</div>

## Sujet

Le projet de fin de semestre de l'UV IA02 est l'implémentation d'une IA pour jouer aux jeux Dodo et Gopher, qui sont deux jeux de plateaux.

Pour en apprendre plus sur le sujet rendez vous [ici](https://gitlab.utc.fr/ia02/gopher-and-dodo).


## Documentation

La documentation se trouve dans le dossier `doc` et est séparée en plusieurs fichiers:

- Le [rendu](doc/RENDU.md) complet du projet
- Sur l'algorithme [MCTS](doc/MCTS.md)
- Sur le hashing efficace de la grille [HASHING](doc/HASHING.md)

## Groupe de projet

&nbsp;&nbsp;&nbsp;· [Chloe Taurel](https://gitlab.utc.fr/taurelch)<br>
&nbsp;&nbsp;&nbsp;· [Gabriel Santamaria](https://gitlab.utc.fr/gsantama)
