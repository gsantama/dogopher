"""
Strategy representation and implementation (minimax, MCTS, etc...)
"""

import time
import os
import sys
from typing import Optional
from copy import deepcopy
from collections import deque
from math import inf
from multiprocessing import cpu_count
from mpire import WorkerPool as Pool
import numpy.random as random

from api import Player, Action
from hexgrid.grid import HexGrid

from game.helpers.mcts import MCTSNode, UCB
from game.helpers.minimax import NegaScout

from . import utils
from .rules import Rules, Dodo, Gopher

sys.setrecursionlimit(10**6)

WORKERS_DIVISOR = 1


class Strategy:
    """
    Base class for a strategy.
    """

    def __init__(self, grid: HexGrid, rules: Rules, ply: Player):
        self.grid = grid
        self.rules = rules
        self.player = ply

    def get_action(self, grid: HexGrid, ply: Player, time_left: int) -> Action:
        """
        Get the action to play for a player.

        Args:
            ply: The player

        Returns:
            The action to play
        """
        raise NotImplementedError


class RandomStrategy(Strategy):
    """
    Random strategy.

    This is the simplest strategy, it chooses a random action from the
    possible and alloweds moves according to a rule.
    """

    def get_action(self, grid: HexGrid, ply: Player) -> Action:
        moves = self.rules.get_legal_moves(grid, ply)
        if moves == []:
            return None

        return moves[random.randint(0, len(moves))]


class StrategyAlphaBeta(Strategy):
    """
    Implementation of the minimax with Alpha-Beta pruning
    """

    def __init__(
        self,
        grid: HexGrid,
        rules: Rules,
        ply: Player,
        depth: int = 3,
    ):
        super().__init__(grid, rules, ply)
        self.depth = depth
        self.memo = {}
        self.current_state = None

    def minmax(
        self,
        ply: Player,
        alpha: float,
        beta: float,
        depth: int = 30,
    ) -> tuple[float, Optional[Action]]:
        """
        The actual Minimax search algorithm, with Alpha-Beta pruning.

        Args:
            grid: The grid
            ply: The player
            alpha: The alpha value
            beta: The beta value
            depth: The depth of the search

        Returns:
            The best score and the best action based on the minimax algorithm
        """

        if depth == 0 or self.rules.game_over(self.current_state):
            score = self.rules.score(self.current_state)
            self.memo[self.current_state] = score
            return score, None

        best_score = -inf if ply == 1 else inf
        best_action = None

        for action in self.rules.get_legal_moves(self.current_state, ply):
            self.current_state.move(action, ply)

            current_score = None
            if self.current_state in self.memo:
                current_score = self.memo[self.current_state]
            else:
                current_score, _ = self.minmax(utils.other(ply), alpha, beta, depth - 1)

            self.current_state.undo(action)

            if ply == 1:  # Maximizing player
                if current_score > best_score:
                    best_score = current_score
                    best_action = action
                alpha = max(alpha, best_score)
            else:  # Minimizing player
                if current_score < best_score:
                    best_score = current_score
                    best_action = action
                beta = min(beta, best_score)

            if alpha >= beta:
                break

        self.memo[self.current_state] = best_score
        return best_score, best_action

    def precompute_first_move(self, grid: HexGrid, ply: Player) -> Action:
        """
        Calculates the most efficient first move for an empty board of the Gopher game, using a very high depth
        This method is only meant to be used once, the results will be manually typed down in the get_first_move method

        Returns:
            The most efficient Gopher Action for the first move of the game
        """
        self.current_state = grid
        return self.minmax(ply, -inf, inf, depth=50)[
            1
        ]  # Use a very high depth for precomputation

    def get_first_move(self) -> Action:
        """
        This function returns the first move for the Gopher game, calculated by the precompute_first_move
        It is meant to save the time wasted by the alphabeta when calculating the very first move of the game, which
        takes a very long time because there are a lot of possibilities when the board is empty.
        Therefore, the first move is calculated beforehand
        Returns:
            The Gopher action of the most efficient first move, depending on the size of the grid

        """
        return -self.grid.size[0] + 1, -self.grid.size[0] + 1

    def get_action(self, grid: HexGrid, ply: Player, _: int = 0) -> Action:
        self.current_state = grid

        action = None
        if self.rules.is_first_turn(grid) and isinstance(self.rules, Gopher):
            action = self.get_first_move()

        if action is not None:
            return action
        return self.minmax(ply, -inf, inf, self.depth)[1]


class NegascoutStrategy(Strategy):
    """
    Implementation of the NegaScout algorithm
    """

    def __init__(self, grid: HexGrid, rules: Rules, ply: Player, depth=3):
        super().__init__(grid, rules, ply)
        self.negascout = NegaScout(rules, depth, ply)

    def get_action(self, grid: HexGrid, ply: Player, _: int = 0) -> Action:
        return self.negascout.get_move(grid, ply)


class MCTSStrategy(Strategy):
    """
    Implementation of the Monte-Carlo Tree Search algorithm.

    Inspired from:
        - https://gist.github.com/qpwo/c538c6f73727e254fdc7fab81024f6e1
        - https://ai-boson.github.io/mcts/

    References:
        - https://www.lri.fr/~sebag/Slides/InvitedTutorial_CP12.pdf
        - [1] Chaslot, Guillaume & Winands, Mark & Herik, H. & Uiterwijk, Jos & Bouzy, Bruno.
          (2008). Progressive Strategies for Monte-Carlo Tree Search.
          New Mathematics and Natural Computation. 04. 343-357. 10.1142/S1793005708001094.
        - [2] H. Baier and M. H. M. Winands, "Monte-Carlo Tree Search and minimax hybrids,"
          2013 IEEE Conference on Computational Inteligence in Games (CIG),
          Niagara Falls, ON, Canada, 2013, pp. 1-8, doi: 10.1109/CIG.2013.6633630.
    """

    # From the statistics got by making two random strategies
    # play agains't each others, in average, 150 moves are made
    # before a game ends. We don't have the time to wait until
    # more than 150 moves for a 6-sized grid in the playout phase
    MAX_DEPTH = 200

    AVG_MAX_MOVES_DODO = 45
    AVG_MAX_MOVES_GOPHER = 30

    MINIMAX_TRIGGER = 7

    # Random number generator
    # Local to each process spawned by the training
    rng: random.Generator = None

    def __init__(
        self,
        grid: HexGrid,
        rules: Rules,
        ply: Player,
        simulations: int = 256,
        exploration_weight: float = 0.3022005658888107,
        threshold: int = 27,
        ucb=UCB.ucb_tuned,
        minimax_level: int = 10,
    ):
        super().__init__(grid, rules, ply)
        untried = rules.get_legal_moves(grid, ply).copy()
        self.size = grid.size[0]
        self.root = MCTSNode(grid, None, None, untried)
        self.simulations = simulations
        self.ce = exploration_weight
        self.threshold = threshold
        self.ucb = ucb
        self.minimax_level = minimax_level
        self.legals = []

        self.niter = 2

        self.gopher = isinstance(rules, Gopher)

        # Number of actions we already played, and the number
        # of time it took in total, we're going to use this to adjust
        # the number of simulations we're going to run
        self.nactions = 1
        self.ttime = 0

        if self.gopher:
            minimax_level = max(2, minimax_level)
            self.MINIMAX_TRIGGER = 2

        self.players: list[StrategyAlphaBeta] = [
            StrategyAlphaBeta(grid, rules, 1, minimax_level),
            StrategyAlphaBeta(grid, rules, 2, minimax_level),
        ]

        self.threshold = (self.simulations // 256) * 37

        self.current_state = deepcopy(grid)

        self.processes = cpu_count()

    def __playout(self, node: MCTSNode, rules: Rules, ply: Player) -> float:
        """
        Playout phase of the MCTS algorithm.

        Args:
            grid: The grid
            rules: The rules of the game
            player: The player

        Returns:
            The result of the game
        """

        mv_stack = deque()
        state = self.current_state
        legals = rules.get_legal_moves(state, ply)
        action = node.action
        current_player = ply

        if action is not None and action in legals:
            state.move(action, ply)
            current_player = utils.other(ply)
            mv_stack.append(action)

        # Citation from [1]:
        # > If the strategy is too stochastic (e.g., if it selects moves nearly randomly),
        # > then the moves played are often weak, and the level of the Monte-Carlo program
        # > is decreasing. In contrast, if the strategy is too deterministic (e.g., if the
        # > selected move for a given position is almost always the same, i.e., too much
        # > exploitation takes place) then the exploration of the search space becomes too
        # > selective, and the level of the Monte-Carlo program is decreasing too.
        #
        # Thus, the two players in the rollout phase are AlphaBeta players with a fixed depth
        # Note: the original idea was found by me, but after further research I found [2] that
        # describes a clever way of doing this (way more clever than what I wanted to do first).
        # The points that they talk about are the use of AlphaBeta algorithms not only in the
        # playout phase, but also in the selection, expansion and backpropagation phases

        depth = 0
        while True:
            if depth >= self.MAX_DEPTH:
                break

            legals = rules.get_legal_moves(state, current_player)

            if not legals:
                break

            action = None

            # Before using the Minimax strategy to choose the move,
            # we check that the branching factor isn't too high to
            # prevent the computing time from being to long
            if len(legals) <= self.MINIMAX_TRIGGER and node.visits >= self.threshold:
                strategy: StrategyAlphaBeta = self.players[current_player - 1]

                res = strategy.get_action(state, current_player)
                if res is not None:
                    action = res[1]

            if action is None:
                i = self.rng.choice(range(len(legals)), 1)[0]
                action = legals[i]

            mv_stack.append(action)
            state.move(action, current_player)
            current_player = utils.other(current_player)

            depth += 1

        score = rules.player_score(state, self.player)

        if isinstance(self.rules, Dodo):
            if score == 0:
                pmobility = self.rules.mobility(state, self.player)
                emobility = self.rules.mobility(state, utils.other(self.player))
                score = (
                    emobility
                    - pmobility
                    - self.rules.blocking(state, ply, utils.other(self.player))
                )

            if score == -1:
                pmobility = self.rules.mobility(state, self.player)
                score = -pmobility if pmobility > 0 else -8

            if score == 1:
                emobility = self.rules.mobility(state, utils.other(self.player))
                score = emobility if emobility > 0 else 8

        while mv_stack:
            state.undo(mv_stack.pop())

        return score

    def __select(self, node: MCTSNode) -> MCTSNode:
        """
        Select a child node to explore.

        If the node has been visited enough, we use the UTC to choose the best child.
        If not, we choose the next child according to our playout strategy.

        Args:
            node: The node to start selection from

        Returns:
            The selected node
        """
        current_node = node
        while current_node.is_expanded():
            current_node = current_node.best_child(self.ce, ucb=self.ucb)

        return current_node

    def __expand(self, node: MCTSNode, rules: Rules):
        """
        Expansion strategy for the MCTS algorithm.

        We're following the strategy from the paper given in reference above.

        We're expanding one node per simulation, chosen by following the selection
        strategy.

        If the node visit count exceed the threshold, we expand all the children of
        this node.

        Args:
            rules: The rules of the game.
            player: The player.
        """
        ply = self.player
        legals = self.legals.copy()

        if node.untried_actions == [] and not node.is_expanded():
            node.untried_actions = legals
        elif not node.is_expanded():
            legals = [action for action in legals if action in node.untried_actions]
            if len(legals) == 0:
                node.untried_actions = legals
        else:
            if node.is_expanded() and len(legals) != 0:
                node.untried_actions = legals
                node.expanded = False
            elif len(legals) == 0:
                legals = []
            else:
                node.untried_actions = legals
                node.expanded = False

        if node.visits >= self.threshold and legals:
            # Before expanding all the nodes by default,
            # we're checking if it's (computing-time wise)
            # possible to use the Minimax strategy to choose
            # which nodes to expands and which ones to not
            if self.MINIMAX_TRIGGER >= len(legals):
                strategy: StrategyAlphaBeta = self.players[ply - 1]
                state = self.current_state

                best = -inf
                baction = None

                for i, action in enumerate(legals):
                    state.move(action, ply)
                    strategy.current_state = state
                    score, _ = strategy.minmax(
                        utils.other(ply), -inf, inf, self.minimax_level
                    )
                    perspective = utils.perspective(ply)
                    if perspective * score > best:
                        best = perspective * score
                        baction = action
                    state.undo(action)

                # If we find a proven loss, then we might not consider
                # this node as a node to expand
                state.move(baction, ply)
                untried = rules.get_legal_moves(state, ply).copy()
                new_child: MCTSNode = MCTSNode(deepcopy(state), node, baction, untried)
                node.children[baction] = new_child
                state.undo(baction)
                node.untried_actions.remove(baction)
                if node.untried_actions == []:
                    node.untried_actions = None
                    node.expanded = True

            # if we can't use the minimax, then just expand all the nodes
            else:
                for _, move in enumerate(legals):
                    state: HexGrid = self.current_state
                    state.move(move, ply)
                    untried = rules.get_legal_moves(state, ply).copy()
                    new_child: MCTSNode = MCTSNode(deepcopy(state), node, move, untried)
                    node.children[move] = new_child
                    state.undo(move)
                node.untried_actions = None
                node.expanded = True

        elif legals:
            n = None
            if not self.gopher:
                moves, probabilities = self.rules.get_moves_distribution(
                    self.current_state, self.player, legals
                )
                n = min(self.niter, len(moves))
                n = self.rng.choice(
                    range(len(moves)), n, p=probabilities, replace=False
                )
            else:  # For the Gopher game, since the get_legals isn't optimized, we're going
                # to use a default uniform choice distribution
                n = min(self.niter, len(legals))
                n = self.rng.choice(range(len(legals)), n, replace=False)
            for i in n:
                action = legals[i - 1]
                state: HexGrid = self.current_state
                state.move(action, ply)
                untried: list[Action] = rules.get_legal_moves(state, ply).copy()
                new_child: MCTSNode = MCTSNode(state, node, action, untried)
                node.children[action] = new_child
                node.untried_actions.remove(action)
                state.undo(action)

                if node.untried_actions == []:
                    node.untried_actions = None
                    node.expanded = True

            # If the visits count is below the threshold, we're going
            # to only expand one child of the node, choosen with a uniform
            # probability, as described in [1]
            if self.niter > 1:
                self.niter -= 1

    def __backpropagate(self, node: MCTSNode, result: float) -> None:
        """
        Backpropagation phase of the MCTS algorithm.

        Args:
            node: The node
            result: The result of the game
        """
        while node is not None and isinstance(node, MCTSNode):
            node.visits += 1
            node.value += result
            node = node.parent

    def __train(self) -> MCTSNode:
        """
        Performs a training of the MCTS tree.
        """
        root = self.root
        node = self.__select(root)
        self.__expand(node, self.rules)
        reward = self.__playout(node, self.rules, self.player)
        self.__backpropagate(node, reward)

        return root

    def train(self, simulations):
        """
        Train a given number of simulations

        Args:
            simulations: The number of simulations to run
        """

        # We need to re-init the see for the
        # random number generators so that
        # it's not always the same nodes expanded
        # for each simulation accross the different
        # processes
        seed = random.SeedSequence(int.from_bytes(os.urandom(16), "big") * os.getpid())
        rng = random.Generator(random.MT19937(seed))
        self.rng = rng

        for _ in range(simulations):
            self.__train()
        return self.root

    def get_action(
        self, grid: HexGrid, ply: Player = None, time_left: int = 0
    ) -> Action:
        """
        Get the action to play for a player using MCTS.

        Args:
            grid: The game grid
            rules: The rules of the game
            ply: The player

        Returns:
            The action to play
        """
        start_time = time.time()

        self.current_state = deepcopy(grid)
        self.legals = self.rules.get_legal_moves(grid, self.player)

        # The total time we have is 300 for all our moves
        if self.nactions != 0:
            avg = self.ttime / self.nactions
            left = (
                self.AVG_MAX_MOVES_DODO - avg
                if not self.gopher
                else self.AVG_MAX_MOVES_GOPHER - avg
            )

            if time_left > left * avg:
                self.simulations = max(512, self.simulations)

            else:
                self.simulations = min(256, self.simulations)

        # Filter out the not available moves anymore from
        # the children of the root node
        to_expand = False
        for action in list(self.root.children.keys()):
            if action not in self.legals:
                to_expand = True
                del self.root.children[action]

        # If we needed to delete some children, we then need to
        # re-expand the node to discover more moves and their values
        if to_expand:
            self.root.expanded = False
            self.root.untried_actions = self.legals
            self.__expand(self.root, self.rules)

        nworkers = (
            cpu_count() // WORKERS_DIVISOR
        )  # Number of workers we're going to use
        sims_per_worker = self.simulations // nworkers
        nodes = []
        with Pool(nworkers) as pool:
            nodes = pool.map(self.train, [sims_per_worker] * nworkers)

        # We then need to merge every results inside our main
        # process memory (we're not using a shared memory system),
        # each spawned process has its own copy of the self.root node
        # and works on it.
        for node in nodes:
            self.root.merge(node)

        # When choosing the best child, we need to also ensure
        # that the move is a legal one
        best_child: MCTSNode = self.root.best_child(self.ce, ucb=self.ucb)
        self.root = best_child.parent

        end_time = time.time()

        self.ttime += end_time - start_time
        self.nactions += 1
        return best_child.action


class BrainStrategy(Strategy):
    """
    Implementation of the human brain strategy
    """

    def get_action(self, grid: HexGrid, ply: Player) -> Action:
        """
        Asks the user what move they want to play

        Args:
            grid: HexGrid
            ply: Player

        Returns:
            Action desired by the user
        """

        def get_int_input(prompt):
            while True:
                value = input(prompt)
                try:
                    return int(value)
                except ValueError:
                    print("Invalid input. Please enter an integer.")

        print("Legal moves : ")
        print(self.rules.get_legal_moves(grid, ply))
        print("Please insert:")

        if isinstance(self.rules, Dodo):
            origin_cell_x = get_int_input(
                "Horizontal coordinates of origin cell of desired action: "
            )
            origin_cell_y = get_int_input(
                "Vertical coordinates of origin cell of desired action: "
            )
            move_cell_x = get_int_input(
                "Horizontal coordinates of landing cell of desired action: "
            )
            move_cell_y = get_int_input(
                "Vertical coordinates of landing cell of desired action: "
            )

            while not (
                ((origin_cell_x, origin_cell_y), (move_cell_x, move_cell_y))
                in self.rules.get_legal_moves(grid, ply)
            ):
                print("This move is not legal. Please try another.")
                origin_cell_x = get_int_input(
                    "Horizontal coordinates of origin cell of desired action: "
                )
                origin_cell_y = get_int_input(
                    "Vertical coordinates of origin cell of desired action: "
                )
                move_cell_x = get_int_input(
                    "Horizontal coordinates of landing cell of desired action: "
                )
                move_cell_y = get_int_input(
                    "Vertical coordinates of landing cell of desired action: "
                )

            return (origin_cell_x, origin_cell_y), (move_cell_x, move_cell_y)

        if isinstance(self.rules, Gopher):
            move_cell_x = get_int_input(
                "Horizontal coordinates of landing cell of desired action: "
            )
            move_cell_y = get_int_input(
                "Vertical coordinates of landing cell of desired action: "
            )

            while not (
                (move_cell_x, move_cell_y) in self.rules.get_legal_moves(grid, ply)
            ):
                print("This move is not legal. Please try another.")
                move_cell_x = get_int_input(
                    "Horizontal coordinates of landing cell of desired action: "
                )
                move_cell_y = get_int_input(
                    "Vertical coordinates of landing cell of desired action: "
                )

            return move_cell_x, move_cell_y

        return None
