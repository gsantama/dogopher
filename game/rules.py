"""
Basic representation of the rules of a game using a HexGrid.
"""

from copy import deepcopy
from typing import Optional, Union
import numpy as np
from api import Player, Action
from hexgrid.grid import HexGrid

from .utils import other, get_direction

MAX_CACHE_DODO = 8192
MAX_CACHE_GOPHER = 1024


class Rules:
    """
    Abstract class for the rules of a game.
    """

    @staticmethod
    def move_repr() -> Action:
        """
        Returns an example of a move

        Returns:
            An example of a move of the move
        """

    @staticmethod
    def is_legal(grid: HexGrid, action: Action, ply: Player) -> bool:
        """
        Check if an action is legal for a player.

        Args:
            grid: The game grid
            action: The action to check
            ply: The player

        Returns:
            True if the action is legal, False otherwise
        """

    @staticmethod
    def get_legal_moves(grid: HexGrid, ply: Player) -> list[Action]:
        """
        Gets the legal moves for a player.

        Args:
            grid: The game grid
            ply: The player

        Returns:
            A list of legal moves
        """

    @staticmethod
    def has_won(grid: HexGrid, ply: Player) -> bool:
        """
        Check if a player has won the game.

        Args:
            grid: The game grid
            ply: The player

        Returns:
            True if the player has won, False otherwise
        """

    @staticmethod
    def game_over(grid: HexGrid) -> bool:
        """
        Check if the game is over.

        Args:
            grid: The game grid

        Returns:
            True if the game is over, False otherwise
        """

    def score(self, grid: HexGrid) -> float:
        """
        Returns the final score of a game
        Args:
            grid: The game grid

        Returns:
            1 if red (1) has won (maximizing player)
            -1 if blue (2) has won (minimizing player)
            0 if draw
        """

    @staticmethod
    def get_moves_distribution(
        grid: HexGrid, ply: Player, f: Optional[list[Action]] = None
    ) -> tuple[list[Action], list[float]]:
        """
        Returns the distribution probabilities of the moves for a player.

        Args:
            grid: The game grid
            ply: The player
            moves: The list of moves we need to get the distribution of
            f: filters the moves that we need to get the distribution of

        Returns:
            A tuple of lists containing the actions and their probabilities to be done
            according to a certain strategy
        """

    @staticmethod
    def is_quiet(grid: HexGrid, ply: Player, move: Action) -> bool:
        """
        A function used to detect quiet moves ie. moves that could lead
        to a loose if played, or moves that could lead to a game state in
        favor of the opponent.

        Args:
            grid: The current game state.
            ply: The player that has to do the move
            move: The move to check.

        Returns:
            True if the move is quiet, False otherwise.
        """

    @staticmethod
    def player_score(grid: HexGrid, ply: Player) -> float:
        """
        Returns the score of a player based on some little algorithms
        Args:
            grid: The game grid
            ply: The player

        Returns:
            A float representing the score of the player
        """

    def is_first_turn(self, grid: HexGrid) -> bool:
        """
        Returns whether or not it is the very first move of the game (if anyone has already played)

        Args:
            grid: The game grid

        Returns:
            A boolean indicating if it's the first move or not
        """

        return grid.last_played == 0

    @staticmethod
    def clear_cache() -> None:
        """
        Clear all the cached data for these rules
        """


class Dodo(Rules):
    """
    Rules for the Dodo game.
    """

    __legals = {}

    def __init__(self):
        Dodo.__legals = {}

    @staticmethod
    def move_repr() -> Action:
        return ((0, 0), (0, 1))

    @staticmethod
    def is_legal(grid: HexGrid, action: Action, ply: Player) -> bool:
        if grid[action[0]] != ply:  # Player can't move a cell that he doesn't own
            return False

        direction = get_direction(ply)

        fneighbours = [
            cell
            for cell in grid.get_neighbours(action[0], direction)
            if grid[cell] == 0
        ]  # Forward unoccupied neighbours

        return action[1] in fneighbours

    @staticmethod
    def get_legal_moves(grid: HexGrid, ply: Player) -> list[Action]:
        if (grid, ply) in Dodo.__legals:
            return Dodo.__legals[(grid, ply)]

        direction = get_direction(ply)

        actions = [
            (tuple(cell), tuple(neighbour))
            for cell in grid.get_cells(ply)
            for neighbour in grid.get_neighbours(cell, direction)
            if grid[neighbour] == 0
        ]

        # We now need to cache the results of the legals
        # of this player for this grid.
        Dodo.__legals[(grid, ply)] = actions

        return actions

    @staticmethod
    def has_won(grid: HexGrid, ply: Player) -> bool:
        last = grid.to_play() == ply  # it's the turn of the player
        return Dodo.get_legal_moves(grid, ply) == [] and last

    @staticmethod
    def game_over(grid: HexGrid) -> bool:
        return Dodo.has_won(grid, 1) or Dodo.has_won(grid, 2)

    def score(self, grid: HexGrid) -> float:
        last = grid.to_play()
        legals = [Dodo.get_legal_moves(grid, ply) for ply in [1, 2]]
        if last == 1 and legals[0] == []:
            return 1
        if last == 2 and legals[1] == []:
            return -1

        return 0

    @staticmethod
    def mobility(
        grid: HexGrid, ply: Player, move: Optional[Action] = None
    ) -> Union[tuple[int, int], int]:
        """
        Returns the number of legal moves after a certain move.
        If the given move is None, it returns the number of legal moves

        In the Dodo game, the less legals move a Player has, the better.
        Also, the more legal moves the ennemy has, the better !

        The ultimate goal is to have 0 legal moves for us, and that the ennemy
        has a maximum of legal moves.

        Args:
            grid: The current game state.
            ply: The player to calculate mobility for.

        Returns:
            The number of legal moves for the player (which is its mobility score)
            after the given move
        """
        if move is not None:
            grid.move(move, ply)

            ply_moves = len(Dodo.get_legal_moves(grid, ply))
            enn_moves = len(Dodo.get_legal_moves(grid, other(ply)))

            grid.undo(move)
            return ply_moves, enn_moves

        return len(Dodo.get_legal_moves(grid, ply))

    @staticmethod
    def blocking(
        grid: HexGrid, ply: Player, ennemy: Player, move: Optional[Action] = None
    ) -> float:
        """
        Returns the blocking potential score for a certain move.

        In the dodo game, the more we can block our ennemy, the worse it is because
        the ultimate goal is to have 0 legal moves, and to maximise the ennemy's
        number of legal moves.

        Args:
            grid: The current game state.
            ply: The player we want to calculate blocking potential score.
            ennemy: The ennemy.

        Returns:
            The blocking potential score after the given move
        """
        blocking_score = 0
        if move:
            grid.move(move, ply)

        # Ennemy mobility just after the move
        mennemy = Dodo.mobility(grid, ennemy)

        # To get the potentiality of blocking moves, we need to know
        # the legals moves of the current player and check the mobility
        # of the ennemy after these moves. Then, we can compute the blocking
        # score which is the number of the moves that results in a decrease of the
        # possible moves of the ennemy (or its mobility score)
        ngrid = deepcopy(grid)
        for legal in Dodo.get_legal_moves(ngrid, ply):
            ngrid.move(legal, ply)

            opponent_moves_after = Dodo.get_legal_moves(ngrid, ennemy)
            if len(opponent_moves_after) < mennemy:
                blocking_score += 1

            ngrid.undo(legal)

        if move:
            grid.undo(move)

        return blocking_score

    @staticmethod
    def get_moves_distribution(
        grid: HexGrid, ply: Player, f: Optional[tuple[Action]] = None
    ) -> tuple[list[Action], list[float]]:

        if f:
            moves = [move for move in Dodo.get_legal_moves(grid, ply) if move in f]
        else:
            moves = Dodo.get_legal_moves(grid, ply)

        if len(moves) == 0:
            return [], []

        probabilities = []

        for move in moves:
            pmobility, emobility = Dodo.mobility(grid, ply, move)
            blocking = Dodo.blocking(grid, ply, other(ply), move)
            probabilities.append(
                1
                + 50000 * ((emobility + 1) / ((pmobility + 1)))
                - 5000 * (1 / (1 + blocking))
            )

        probabilities = np.array(probabilities)
        # We're just making sure there are no probabilities equal to 0
        probabilities = np.abs(probabilities) + 1

        tot = sum(probabilities)
        if tot == 0:
            # If the total is zero, we're backing up to a
            # uniform distribution
            probabilities = np.ones(len(moves))
            return moves, probabilities * (1 / len(moves))
        return moves, probabilities / tot

    @staticmethod
    def is_quiet(grid: HexGrid, ply: Player, move: Action) -> bool:
        grid.move(move, ply)

        legals = Dodo.get_legal_moves(grid, other(ply))

        # This moves leads us to a direct
        # loss, thus it's a quiet one
        if legals == []:
            grid.undo(move)
            return True

        grid.undo(move)
        return False

    @staticmethod
    def player_score(grid: HexGrid, ply: Player) -> float:
        ennemy = other(ply)

        # We first check if someone has won
        if Dodo.has_won(grid, ply):
            return 1
        if Dodo.has_won(grid, ennemy):
            return -1

        return 0

    @staticmethod
    def clear_cache():
        Dodo.__legals = {}


class Gopher(Rules):
    """
    Rules for the Gopher game.
    """

    @staticmethod
    def move_repr() -> Action:
        return (0, 0)

    @staticmethod
    def is_legal(grid: HexGrid, action: Action, ply: Player) -> bool:
        if grid.is_empty and ply == 1:  # First move of the game
            return True

        if grid[action] != 0:  # Player can't place a cell if it's not empty
            return False

        neighbours = grid.get_neighbours(action)  # All neighbours of the cell
        # To be a legal move, it needs to form exactly ONE ennemy connection
        # and ZERO friendly connection

        if sum(grid[cell] == ply for cell in neighbours) > 0:
            return False

        ennemy = other(ply)

        if sum(grid[cell] == ennemy for cell in neighbours) != 1:
            return False

        return True

    @staticmethod
    def get_legal_moves(grid: HexGrid, ply: Player) -> list[Action]:
        if grid.is_empty:
            return [tuple(cell) for cell in grid.get_cells(0)]

        ennemy = other(ply)
        moves = []

        for cell in grid.get_cells(0):
            neighbours = grid.get_neighbours(tuple(cell))
            if sum(grid[cell] == ply for cell in neighbours) > 0:
                continue

            if sum(grid[cell] == ennemy for cell in neighbours) != 1:
                continue

            moves.append(tuple(cell))

        return moves

    @staticmethod
    def has_won(grid: HexGrid, ply: Player) -> bool:
        if grid.is_empty and ply == 1:  # First move of the game
            return False

        # A player won a party if it's the last one to put a stone
        # and to have a possibility to play

        return (
            Gopher.get_legal_moves(grid, other(ply)) == [] and grid.last_played == ply
        )

    @staticmethod
    def game_over(grid: HexGrid) -> bool:
        return Gopher.has_won(grid, 1) or Gopher.has_won(grid, 2)

    def score(self, grid: HexGrid) -> float:
        if self.has_won(grid, 1):
            return 1
        if self.has_won(grid, 2):
            return -1
        return 0

    @staticmethod
    def get_moves_distribution(
        grid: HexGrid, ply: Player, f: Optional[list[Action]] = None
    ) -> tuple[list[Action], list[float]]:

        moves = Gopher.get_legal_moves(grid, ply)
        if f is not None:
            moves = f

        probabilities = [1 / len(moves) for _ in moves]

        return moves, probabilities

    @staticmethod
    def is_quiet(grid: HexGrid, ply: Player, move: Action) -> bool:
        if grid.is_empty:
            return False

        grid.move(move, ply)
        ennemy = other(ply)
        if Gopher.has_won(grid, ennemy):
            grid.undo(move)
            return True

        grid.undo(move)
        return False

    @staticmethod
    def player_score(grid: HexGrid, ply: Player) -> float:
        ennemy = other(ply)
        # We first check if someone has won
        if Gopher.has_won(grid, ply):
            return 1
        if Gopher.has_won(grid, ennemy):
            return -1
        return 0

    @staticmethod
    def clear_cache() -> None:
        pass
