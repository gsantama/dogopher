"""
Implementation of Nodes and various useful stuff for the MCTS algorithm.
"""

from typing import Callable, Dict
from math import log

from api import Action
from hexgrid.grid import HexGrid


class UCB:
    """
    Collection of Upper Confidence Bound (UCB) calculations

    References:
        - https://en.wikipedia.org/wiki/Monte_Carlo_tree_search#Exploration_and_exploitation
        - https://www.lri.fr/~sebag/Slides/InvitedTutorial_CP12.pdf
    """

    @staticmethod
    def ucb(node: "MCTSNode", ce: float) -> float:
        """
        Compute the UCB value of a given node

        Args:
            node: The node.
            ce: The exploration weight.

        Returns:
            The UCB value.
        """
        if node.visits == 0:
            return float("inf")

        return (
            node.value / node.visits
            + (ce * log(node.parent.visits) / node.visits) ** 0.5
        )

    @staticmethod
    def ucb_tuned(node: "MCTSNode", ce: float) -> float:
        """
        Compute the UCB-tuned value of a given node.

        The UCB-Tuned method is a variation of the UCB that takes
        take into account the standard deviation of µ_i.

        For references, please see the document at this address:
        https://www.lri.fr/~sebag/Slides/InvitedTutorial_CP12.pdf

        Args:
            node: The node.
            ce: The exploration weight.

        Returns:
            The UCB value.
        """

        if node.visits == 0:
            return float("inf")

        mean = node.value / node.visits
        variance = ((node.value**2) / node.visits) - mean**2

        tune = min(1 / 4, variance + (2 * log(node.parent.visits) / node.visits) ** 0.5)

        return mean + (ce * (log(node.parent.visits) / node.visits) + tune) ** 0.5


class Dummy:
    """
    A dummy node for the root of the MCTS tree
    """

    def __init__(self):
        self.parent = None
        self.children = {}
        self.value: float = 0.0
        self.visits: int = 0


class MCTSNode:
    """
    Monte-Carlo Search Tree algorithms and data structure.

    Inspired from:
        - https://gist.github.com/qpwo/c538c6f73727e254fdc7fab81024f6e1
        - https://ai-boson.github.io/mcts/

    References:
        - https://www.lri.fr/~sebag/Slides/InvitedTutorial_CP12.pdf
    """

    action: Action = None
    children: Dict[Action, "MCTSNode"] = {}
    parent: "MCTSNode" = None

    value: float = 0.0
    visits: int = 0

    def __init__(self, state: HexGrid, parent=None, action=None, untried=None):
        self.state = state
        self.parent = parent

        if self.parent is None:
            self.depth = 0
            self.parent = Dummy()
        else:
            self.depth = self.parent.depth + 1

        self.children = {}
        self.visits = 0
        self.value = 0.0
        self.untried_actions = untried
        self.action = action
        self.expanded = False

    def is_expanded(self) -> bool:
        """
        Checks if the node is expanded.

        Args:
            rules: The rules of the game.
            player: The player.

        Returns:
            Whether or not the node is expanded
        """
        return self.expanded

    def best_child(
        self,
        exploration_weight: float = 1.4,
        ucb: Callable[["MCTSNode", float], float] = UCB.ucb,
    ):
        """
        Return the child with the highest UCB1 value.

        Args:
            exploration_weight: The exploration weight.
            ucb: The function used to compute the UCB value.

        Returns:
            The best child node.
        """
        return max(
            self.children.values(),
            key=lambda node: ucb(node, exploration_weight),
        )

    def most_visited(self) -> "MCTSNode":
        """
        Return the child with the most visits.

        Returns:
            The child node with the most visits.
        """
        return max(self.children.values(), key=lambda node: node.visits)

    def merge(self, other: "MCTSNode"):
        """
        Merge another MCTSNode into the current node.

        Args:
            other: The other MCTSNode to merge.
        """
        if self.action != other.action:
            raise ValueError("Cannot merge nodes with different actions")

        self.visits += other.visits
        self.value += other.value

        for action, child in other.children.items():
            if action in self.children:
                self.children[action].merge(child)
            else:
                self.children[action] = child
                child.parent = self

        if self.untried_actions is None:
            self.untried_actions = other.untried_actions
        elif other.untried_actions is not None:
            self.untried_actions = list(
                set(self.untried_actions) | set(other.untried_actions)
            )
