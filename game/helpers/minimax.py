"""
Implementation various useful stuff for the Minimax-based algorithms.
"""

from math import inf
from copy import deepcopy
from api import Player, Action
from hexgrid.grid import HexGrid
from game.rules import Rules
from game.utils import other


class Minimax:
    """
    Base class form Minimax-based algorithms
    """

    grid: HexGrid
    rules: Rules
    ply: Player
    cache: dict[HexGrid, float] = {}

    def __init__(self, rules: Rules, depth: int, ply: Player):
        self.rules = rules
        self.depth = depth
        self.ply = ply

    def get_move(self, grid: HexGrid, ply: Player) -> Action:
        """
        Gets the best move for the player according
        for the current grid.

        Args:
            grid: The current grid.
            ply: The player.

        Returns:
            The best move.
        """
        raise NotImplementedError

    def get_score(self, grid: HexGrid, ply: Player, move: Action) -> float:
        """
        Gets the score of a move for the given player.

        Args:
            grid: The current grid.
            ply: The player.
            move: The move.

        Returns:
            The score of the move.
        """

    def quiesce(self, alpha, beta, ply: Player):
        """
        Quiescence search algorithm, used to reduce the impact of the horizon.

        Based on:
            - https://github.com/danthurston/BeatMyChessAI/
              by https://github.com/danthurston (Dan Thurston)

        - References:
            - https://en.wikipedia.org/wiki/Quiescence_search
            - https://www.chessprogramming.org/Quiescence_Search
        """

        q = self.rules.score(self.grid)
        if q >= beta:
            return beta

        if q < alpha:
            return alpha

        alpha = q

        for move in self.rules.get_legal_moves(self.grid, ply):
            if self.rules.is_quiet(self.grid, ply, move):
                self.grid.move(move, ply)
                score = -self.quiesce(-beta, -alpha, other(ply))
                self.grid.undo(move)

                if score >= beta:
                    return beta
                alpha = max(alpha, score)

        return alpha

    def clear_cache(self):
        """
        Clears the cache of the algorithm.
        """
        self.cache = {}


class AlphaBeta(Minimax):
    """
    AlphaBeta algorithm implementation

    """


class NegaScout(Minimax):
    """
    NegaScout algorithm implementation

    Based on:
        - https://github.com/danthurston/BeatMyChessAI/
          by https://github.com/danthurston (Dan Thurston)

    References:
        - https://en.wikipedia.org/wiki/Principal_variation_search
    """

    def negascout(self, alpha: float, beta: float, depth: int, ply: Player) -> float:
        """
        The NegaScout algorithm implementation.

        Args:
            alpha: The alpha value.
            beta: The beta value.
            depth: The depth of the search.

        Returns:
            The best score.
        """
        best = -inf
        b = beta

        if depth == 0:
            return self.quiesce(alpha, beta, ply)

        if self.grid in self.cache:
            return self.cache[self.grid]

        ennemy = other(ply)
        for move in self.rules.get_legal_moves(self.grid, ply):
            self.grid.move(move, ply)
            score = -self.negascout(-b, -alpha, depth - 1, ennemy)

            if score > best:
                if alpha < score < beta:
                    best = max(score, best)
                else:
                    best = -self.negascout(-beta, -score, depth - 1, ennemy)

            self.grid.undo(move)
            alpha = max(score, alpha)
            if alpha >= beta:
                return alpha

            b = alpha + 1

        self.cache[self.grid] = best
        return best

    def get_move(self, grid: HexGrid, ply: Player) -> Action:
        """
        Gets the best move for the player for given current grid.

        Args:
            grid: The current grid.
            ply: The player.

        Returns:
            The best move.
        """

        self.clear_cache()

        self.grid = deepcopy(grid)
        legals = self.rules.get_legal_moves(grid, ply)

        best = -inf
        best_move = legals[0]

        for move in legals:
            grid.move(move, ply)
            score = -self.negascout(-inf, inf, self.depth, other(ply))

            if score > best:
                best = score
                best_move = move

            grid.undo(move)

        return best_move

    def get_score(self, grid: HexGrid, ply: Player, move: Action) -> float:
        self.grid = deepcopy(grid)
        self.grid.move(move, ply)
        score = -self.negascout(-inf, inf, self.depth, other(ply))
        self.grid.undo(move)
        return score
