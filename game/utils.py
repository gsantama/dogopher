"""
Utility fonctions for the game.
"""

from api import Player, Action


def other(ply: Player) -> Player:
    """
    Get the other player.

    Args:
        ply: The player

    Returns:
        The other player
    """
    return 3 - ply


def perspective(ply: Player) -> int:
    """
    Returns the perspective of the player.
    """
    if ply == 1:
        return 1
    else:
        return -1


def get_direction(ply: Player) -> int:
    """
    Get the direction in which the player should play.

    Args:
        ply: The player

    Returns:
        The direction in which the player should play
    """
    return 1 - int(ply == 1)


def get_action_type(action: Action) -> bool:
    """
    Returns the type of the action.

    True means a Dodo action, False a Gopher one
    """
    match action:
        case ((_, _), (_, _)):
            return True
        case (_, _):
            return False
