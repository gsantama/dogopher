#!/bin/bash

output_format="gif" # default format
output_name="output" # default output name

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -gif) output_format="gif" ;;
        -mp4) output_format="mp4" ;;
        -o) output_name="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ "$output_format" == "gif" ]; then
    ffmpeg -i hexgrid_%d.png -vf "palettegen" palette.png
    ffmpeg -i hexgrid_%d.png -i palette.png -filter_complex "paletteuse" "${output_name}.gif"
    rm palette.png
elif [ "$output_format" == "mp4" ]; then
    ffmpeg -framerate 10 -i hexgrid_%d.png -c:v libx264 -pix_fmt yuv420p "${output_name}.mp4"
fi

rm hexgrid_*.png
